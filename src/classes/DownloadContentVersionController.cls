/**
 * Class Name: DownloadContentVersionController
 * Purpose: Controller to provide the Id for the ContentVersion
 * Created By: Hardik Ranjan
 * Last Modified: Hardik Ranjan
 */
public without sharing class DownloadContentVersionController {


    /*
     * Method Name: getDocumentIdByName
     * Description: Method to get ContentVersion Id from Name
     * @return: String
     * @param: fileName
     */
    @AuraEnabled
    public static String getDocumentIdByName(String fileNameToDownload) {
        String documentId = '';

        List<ContentVersion> listOfContentVersions = new List<ContentVersion>();

        System.debug('File Name  : ' + fileNameToDownload);
        List<ContentDocument> documents = [SELECT Id, Title FROM ContentDocument WHERE Title = :fileNameToDownload];

        System.debug('documents: ' + documents);

        if (!documents.isEmpty()) {
            listOfContentVersions = [SELECT Id, VersionData FROM ContentVersion WHERE ContentDocumentId = :documents[0].Id AND IsLatest = TRUE];

            System.debug('listOfContentVersions: ' + listOfContentVersions);
            if(!listOfContentVersions.isEmpty())
                documentId = listOfContentVersions[0].Id;

        }


        return documentId;
    }


}