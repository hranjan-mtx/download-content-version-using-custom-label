/**
 * Created by hardikranjan on 29/02/20.
 */

({
    // Generic method to do apex callout
    doCallout : function(cmp, methodName, params, callBackFunc){
        var action = cmp.get(methodName);
        action.setParams(params);
        action.setCallback(this, callBackFunc);
        $A.enqueueAction(action);
    },


    // Fetch data and Download the file
    fetchDocumentAndDownload : function(component, event, helper) {

        let _fileName = component.get('v.fileName');
        let _sourceOfFile = component.get('v.sourceOfFile');
        let _fileNameparam = '';

        if(_sourceOfFile === 'Custom Label')
            _fileNameparam = $A.get("$Label.c." + _fileName).toString();
        else
            _fileNameparam = _fileName;


        if(_fileNameparam) {

            helper.doCallout(component,"c.getDocumentIdByName",{ fileNameToDownload : _fileNameparam}, function(response){
                let state = response.getState();
                if(state === 'SUCCESS') {
                    var _resp = response.getReturnValue();

                    if(_resp) {
                        helper.downloadFile(component, event, helper, _resp);
                    } else {
                        helper.showToast(component, event, "Failed!","error", "File not found!");
                    }

                } else {
                    helper.showToast(component, event, "Failed!","error", "Unable to download the data!");
                }
            });

        } else {
           helper.showToast(component, event, "Failed!","error", "Please enter the CustomLabel/FileName to download!");
        }
    },


    // Download the content version of the file
    downloadFile : function(component, event, helper, documentId) {

        let _internalOrCommunity = component.get('v.internalOrCommunity');
        let _urlSeperator = '';
        let _url = window.location.href;

        try {
            if(_internalOrCommunity === 'Internal')
                _urlSeperator = '/lightning/';
            else
                _urlSeperator = '/s/';

            // Download URL to download
            window.open(_url.split(_urlSeperator)[0] + '/sfc/servlet.shepherd/version/download/' + documentId +'?');
        } catch (ex) {
            helper.showToast(component, event, "Failed!","error", "Unable to download the file!");
        }
    },


    // Display Toast
    showToast : function(component, event, title , state, msg) {
        component.find('notifLib').showToast({
            "title": title,
            "variant": state,
            "message": msg
        });
    }

});