/**
 * Created by hardikranjan on 29/02/20.
 */

({
    // Init handler to fetch data and download the file
    doInit : function(component, event, helper) {
        helper.fetchDocumentAndDownload(component, event, helper);
    },
});